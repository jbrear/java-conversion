﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JavaConversion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            init();
            start();           
        }
        #region HSB
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get { return h; }
            }

            public float S
            {
                get { return s; }
            }

            public float B
            {
                get { return b; }
            }

            public int A
            {
                get { return a; }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;

                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        #endregion

	private const int MAX = 256;      // max iterations
	private const double SX = -2.025; // start value real
    private const double SY = -1.125; // start value imaginary
	private const double EX = 0.6;    // end value real
	private const double EY = 1.125;  // end value imaginary
	private static int x1, y1, xs, ys, xe, ye;
	private static double xstart, ystart, xende, yende, xzoom, yzoom;
	private static Boolean action, rectangle, finished;
	private static float xy;
	private Graphics g1, g2;
    private Bitmap picture, box; //picture for the fractal and area for the area box
    int fractalcol = 0;

    public void init() // all instances will be prepared
    {
        //this.Size = new Size(640,480); //set in properties
        finished = false;
        x1 = 640;
        y1 = 480;
        xy = (float)x1 / (float)y1;
        picture = new Bitmap(x1, y1);
        box = new Bitmap(x1, y1);
        g1 = Graphics.FromImage(picture);
        g2 = Graphics.FromImage(box);
        finished = true;
        this.DoubleBuffered = true;
    }

	public void destroy() // delete all instances 
	{
		if (finished)
		{
			picture = null;
			g1 = null;
		}
	}

	public void start()
	{
		action = false;
		rectangle = false;
		initvalues();
		xzoom = (xende - xstart) / (double)x1;
		yzoom = (yende - ystart) / (double)y1;
		mandelbrot();
	}
	
	public void paint(Graphics g1)
	{
		update(g1);//paint to screen
      
	}
	
	public void update(Graphics g1)
	{
		
        
        
      g1.DrawImage(picture,0,0);//draw g1 to screen
		if (rectangle)
		{	
            Pen pwhite = new Pen(Color.White);//create and set pen to white
			if (xs < xe)
			{
                if (ys < ye) g1.DrawRectangle(pwhite,xs, ys, (xe - xs), (ye - ys));
                else g1.DrawRectangle(pwhite, xs, ye, (xe - xs), (ys - ye));
			}
			else
			{
                if (ys < ye) g1.DrawRectangle(pwhite, xe, ys, (xs - xe), (ye - ys));
                else g1.DrawRectangle(pwhite, xe, ye, (xs - xe), (ys - ye));
			}
		}
	}

    private void mandelbrot() // calculate all points
    {
        int x, y;
        float h, b, alt = 0.0f;
        Pen draw = new Pen(Color.Black);
        action = false;

        for (x = 0; x < x1; x += 2)
            for (y = 0; y < y1; y++)
            {
                h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y, fractalcol); // color value
                
                if (h != alt)
                {
                    b = 1.0f - h * h; // brightness
                    HSBColor HSBCol = new HSBColor(h*255, 0.8f*255, b*255); //create new hsb color
                   Color col = HSBColor.FromHSB(HSBCol); // set color to be the hsb color
                     draw = new Pen(col); // change the pen to be the color calculated by the mandelbrot equation
                    alt = h;
                   
                }
                g1.DrawLine(draw,x, y, x + 1, y);//draw a dot on the screen
                
            }
        action = true;
    }
	
	private float pointcolour(double xwert, double ywert, int fractalcol) // color value from 0.0 to 1.0 by iterations
	{
		double r = 0.0, i = 0.0, m = 0.0;
		double j = fractalcol ;//variable to change color used when drawing fractal
		
		while ((j < MAX) && (m < 4.0))
		{
            j = j +1;
			m = r * r - i * i;
			i = 2.0 * r * i + ywert;
			r = m + xwert;
		}
		return (float)j / (float)MAX;
	}
	
	private void initvalues() // reset start values
	{
		xstart = SX;
		ystart = SY;
		xende = EX;
		yende = EY;
		if ((float)((xende - xstart) / (yende - ystart)) != xy ) 
			xstart = xende - (yende - ystart) * (double)xy;
	}

	
	


        private void Form1_Load(object sender, EventArgs e)
    {
      

        }

  

        private void Form1_Paint_1(object sender, PaintEventArgs e)
        {
            Graphics g1 = e.Graphics; // assign g1 graphics
            Graphics g2 = e.Graphics;//assign g2 graphics
            g1.DrawImageUnscaled(picture, 0, 0);
            g2.DrawImageUnscaled(box, 0, 0);
            update(g1);
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (action)
                {
                    xe = e.X;
                    ye = e.Y;
                    rectangle = true;                
                    Refresh();
                }
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
		int z, w;
		if (action)
		{
		xe = e.X;
		ye = e.Y;
		if (xs > xe)
		{
		z = xs;
		xs = xe;
		xe = z;
		}
		if (ys > ye)
		{
		z = ys;
		ys = ye;
		ye = z;
		}
		w = (xe - xs);
		z = (ye - ys);
		if ((w < 2) && (z < 2)) initvalues();
		else
		{
		if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
		else xe = (int)((float)xs + (float)z * xy);
		xende = xstart + xzoom * (double)xe;
		yende = ystart + yzoom * (double)ye;
		xstart += xzoom * (double)xs;
		ystart += yzoom * (double)ys;
		}
		xzoom = (xende - xstart) / (double)x1;
		yzoom = (yende - ystart) / (double)y1;
		mandelbrot();
		rectangle = false;
        Refresh();
		}
    }

        private void menuStrip1_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Arrow;//changes cursor to be arrow when on title bar
        }

        private void Form1_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Cross;//changes cursor to be crosshairs when on the form
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();//new save dialog object
            sfd.Title = "Save the Fractal";//title of the box
            sfd.Filter = "Bitmap Image| *.bmp";//type of file to save
            sfd.ShowDialog();//show the save dialog

            if (sfd.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();
                picture.Save(fs,System.Drawing.Imaging.ImageFormat.Bmp);//save the picture to file
                fs.Close();//close filestream
            }          
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextReader os = new StreamReader("State.txt"); //create new textreader
            string xstarts = os.ReadLine();//read contents of file and assign to variables
            string xzooms = os.ReadLine();
            string ystarts = os.ReadLine();
            string yzooms = os.ReadLine();
            string fractalcols = os.ReadLine();
            xstart = Convert.ToDouble(xstarts);//change vairaibles of the program to be the ones inthe file
            xzoom = Convert.ToDouble(xzooms);
            ystart = Convert.ToDouble(ystarts);
            yzoom = Convert.ToDouble(yzooms);
            fractalcol = Convert.ToInt32(fractalcols);
            os.Close();//close reader
            mandelbrot();//calculate new fractal with given variables
            Refresh();//update screen
        }

   

        private void saveStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextWriter ss = new StreamWriter("State.txt");//new writer
            ss.WriteLine(xstart);//write the variables to new lines in a text file
            ss.WriteLine(xzoom);
            ss.WriteLine(ystart);
            ss.WriteLine(yzoom);
            ss.WriteLine(fractalcol);
            ss.Close();//close writer
        }

        private void redToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            fractalcol = -50;// change color to red
            mandelbrot();
            Refresh();
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fractalcol = 60;// change color to green
            mandelbrot();
            Refresh();
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fractalcol = 150;//change color to blue
            mandelbrot();
            Refresh();
        }

    

        private void zoomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double zoomdiv = 1.8;
            int i = 0;
           xstart = -1.402;
            while (i<15)// zooms in and pans left 15 times
            {
           xzoom = xzoom / zoomdiv;
           ystart = ystart /zoomdiv;
           yzoom = yzoom / zoomdiv;
            mandelbrot();
            Refresh();
            i++;
            }
        }

        private void cycleToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            while (fractalcol < 200)//cycles colors using the mandelbrot equastion
            {
                fractalcol = fractalcol + 10;
                mandelbrot();
                Refresh();

            }
            fractalcol = 0;
            mandelbrot();
            Refresh();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fractalcol = 0;//reset fractal color
            mandelbrot();
            Refresh();
        }

        private void quit() { 
 
        }


        private void customToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int j = 1;
      

            while (j < 19)
            {

                int i = 1;
                int cycle = 0;
                System.IO.MemoryStream stream = new System.IO.MemoryStream();//open new memory stream
                picture.Save(stream, ImageFormat.Gif);//save gif to memory
                Bitmap gif = new Bitmap(stream);//new bitmap from 
                ColorPalette palette = gif.Palette;
                while (cycle < 7)
                {

                    while (i < 255)
                    {


                        palette.Entries[i] = palette.Entries[i + 1];//cycle colors
                        gif.Palette = palette;//replace pallete with new palette
                        i = i + 1;
                    }
                    g1.DrawImage(gif, 0, 0);//draw gif to g1
                    Refresh();//draw gif to screen
                    cycle = cycle + 1;
                }
                stream.Close();//close stream
                j++;
            }

            mandelbrot();//refresh image
            Refresh();
        }
        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i =0;//creates 250 pictures that have varying colors to color cycle smoother

            while (i < 250)
            {
                fractalcol = fractalcol + 1;
                mandelbrot();
                picture.Save("fractal"+i+".bmp");
                i++;
            }          
        }

        private void cycleLoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i = 0;

            while (i < 250)// loads the images created
            {
               
             Image picture = Image.FromFile("fractal" + i + ".bmp");
             g1.DrawImage(picture,0,0);
             Refresh();
                i++;
            }
            mandelbrot();
            Refresh();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }
        
        int i = 0;
        
        private void saveImageForAnimationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            picture.Save("anim" + i + ".bmp");//leftovers from creating the animation
            i++;
        }

        private void loadAnimationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i = -10;

            while (i < 46)//loads each animation picture to zoom in to the fractal
            {

                Image picture = Image.FromFile("anim" + i + ".bmp");
                g1.DrawImage(picture, 0, 0);
                Refresh();
                i++;
            }
            mandelbrot();
            Refresh();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome to the help screen \n Use the buttons at the top to perform any actions \n right click at any time to reset");
        }
        }
    }


